---
# Baseline testing from git trees
.report_rule_to_failed_test_maintainers: &report_rule_to_failed_test_maintainers
  when: failed_tests
  send_to: failed_tests_maintainers

.default:
  architectures: 'x86_64 ppc64le aarch64 s390x'
  cki_pipeline_project: cki-trusted-contributors
  kpet_tree_name: rawhide
  builder_image: quay.io/cki/builder-rawhide
  config_target: 'olddefconfig'
  make_target: 'targz-pkg'
  test_set: 'kt1'
  scratch: 'false'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]

.with_selftests:
  # debug kernels are too large for gitlab artifacts
  test_debug: 'true'
  build_selftests: 'true'
  test_set: 'kt1|kself'

.mainline-base:
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  kcidb_tree_name: upstream-mainline
  .branches:
    - master
  cki_pipeline_branch: mainline.kernel.org

mainline.kernel.org:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1/
  .extends: .mainline-base
  domains: 'general|dedicated ampere_altra'
  selftest_subsets: 'net bpf'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'nathan@kernel.org'}

mainline.kernel.org-clang:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/91
  .extends: .mainline-base
  compiler: clang
  selftest_subsets: 'net bpf seccomp lkdtm'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: 'llvm@lists.linux.dev'}

scsi:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/mkp/scsi.git
  kcidb_tree_name: upstream-scsi
  .branches:
    - for-next
  cki_pipeline_branch: scsi
  test_set: 'stor'

arm-next:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/19
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git
  kcidb_tree_name: upstream-arm-next
  .branches:
    - for-kernelci
  cki_pipeline_branch: arm
  architectures: 'aarch64'
  domains: 'general|dedicated ampere_altra'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - when: always
      send_to:
        - will@kernel.org
        - catalin.marinas@arm.com
        - linux-arm-kernel@lists.infradead.org

arm-acpi:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/64
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/lpieralisi/linux.git
  kcidb_tree_name: upstream-arm-acpi
  .branches:
    - acpi/for-next
  cki_pipeline_branch: arm
  architectures: 'aarch64'
  test_set: 'acpi'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: lorenzo.pieralisi@arm.com}

stable:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  kcidb_tree_name: upstream-stable
  .branches:
    - linux-6.10.y
  cki_pipeline_branch: upstream-stable
  .report_rules:
    # don't notify test maintainers about failures
    # https://gitlab.com/cki-project/pipeline-data/-/issues/11
    # - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - {when: always, send_to: stable@vger.kernel.org}
    - {when: always, send_to: jforbes@redhat.com}

stable-queue:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  kcidb_tree_name: upstream-stable-queue
  .branches:
    - queue/6.11
  cki_pipeline_branch: upstream-stable
  .report_rules:
    # don't notify test maintainers about failures
    # https://gitlab.com/cki-project/pipeline-data/-/issues/11
    # - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - {when: always, send_to: stable@vger.kernel.org}
    - {when: always, send_to: jforbes@redhat.com}

.rt-base:
  .extends: .with_selftests
  architectures: 'x86_64'
  rt_kernel: 'True'
  selftest_subsets: 'net livepatch bpf'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: kernel-rt-ci@redhat.com}

rt-devel:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/14
  .extends: .rt-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git
  kcidb_tree_name: upstream-rt-devel
  .branches:
    - linux-6.3.y-rt
  cki_pipeline_branch: rt-devel

rt-stable:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/issues/5
  .extends: .rt-base
  git_url:
    https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-stable-rt.git
  kcidb_tree_name: upstream-rt-stable
  .branches:
    - v6.1-rt
    - v5.15-rt
    - v5.10-rt
    - v4.19-rt
  cki_pipeline_branch: rt-stable

block:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/41
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/axboe/linux-block.git
  kcidb_tree_name: upstream-block
  .branches:
    - for-next
    - for-current
  cki_pipeline_branch: block
  test_set: 'stor|fs'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    # Do not send emails to stable until we discuss requirements with the
    # tree maintainers!
    # - when: failed
    #   send_to:
    #     - linux-block@vger.kernel.org
    #     - axboe@kernel.dk

bpf-next:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/46
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git
  kcidb_tree_name: upstream-bpf-next
  .branches:
    - master
  cki_pipeline_branch: bpf
  architectures: 'x86_64'
  builder_image: quay.io/cki/builder-rawhide-llvm
  test_set: 'bpf'
  selftest_subsets: 'bpf net'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]

kvm:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/77
  .extends: .with_selftests
  git_url: https://git.kernel.org/pub/scm/virt/kvm/kvm.git
  kcidb_tree_name: upstream-kvm
  .branches:
    - master
    - next
  cki_pipeline_branch: kvm
  architectures: 'x86_64'
  test_set: 'virt'
  selftest_subsets: 'kvm'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: jarichte@redhat.com}

gfs2:
  onboard_request: >-
    https://gitlab.com/cki-project/pipeline-data/-/merge_requests/135
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/gfs2/linux-gfs2.git
  kcidb_tree_name: upstream-gfs2
  .branches:
    - for-next
  cki_pipeline_branch: gfs2
  test_set: 'gfs2'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: always, send_to: gfs2-maint@redhat.com}

.netfilter_base:
  cki_pipeline_branch: netfilter
  onboard_request: https://gitlab.com/cki-project/pipeline-data/-/issues/24
  architectures: 'x86_64'
  .branches:
    - main
  test_set: 'netfilter-upstream'
  .report_rules:
    - when: always
      send_to:
        - egarver@redhat.com
        - fwestpha@redhat.com
        - psutter@redhat.com
        - yiche@redhat.com

nf:
  .extends: .netfilter_base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/netfilter/nf.git
  kcidb_tree_name: upstream-nf

nf-next:
  .extends: .netfilter_base
  .branches:
    - main
    - testing
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/netfilter/nf-next.git
  kcidb_tree_name: upstream-nf-next

.net-queue-base:
  cki_pipeline_branch: net-queue
  architectures: 'x86_64'
  .branches:
    - dev-queue
  test_set: 'net-ice'
  concurrency_group: net-queue
  concurrency_limit: 1
  .report_rules:
    - when: always
      send_to:
        # users commented out due to:
        # https://gitlab.com/cki-project/pipeline-data/-/issues/23
        # - kzhang@redhat.com
        # - dkc@redhat.com
        # - ctrautma@redhat.com
        - zfang@redhat.com
        # - jtoppins@redhat.com

printk:
  description: >-
    Run kexec/kdump cases on printk tree to make sure atomic console doesn't
    cause any troubles.
  onboard_request: https://gitlab.com/cki-project/pipeline-data/-/issues/14
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/printk/linux.git
  kcidb_tree_name: upstream-printk
  .branches:
    - for-next
  cki_pipeline_branch: printk
  architectures: 'x86_64 aarch64'
  test_set: kdump
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - {when: failed_tests, send_to: bhe@redhat.com}

tnguy-net:
  onboard_request: https://gitlab.com/cki-project/pipeline-data/-/issues/7
  .extends: .net-queue-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/net-queue.git
  kcidb_tree_name: upstream-tnguy-net

tnguy-next:
  onboard_request: https://gitlab.com/cki-project/pipeline-data/-/issues/7
  .extends: .net-queue-base
  git_url: https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/next-queue.git
  kcidb_tree_name: upstream-tnguy-next

.xfs-base:
  onboard_request: https://gitlab.com/cki-project/pipeline-data/-/issues/21
  git_url: https://git.kernel.org/pub/scm/fs/xfs/xfs-linux.git
  .branches:
    - for-next
  cki_pipeline_branch: xfs
  test_set: xfs-upstream
  .report_rules:
    # don't notify test maintainers about failures
    - {when: failed_tests, send_to: cmaiolin@redhat.com}

xfs:
  .extends: .xfs-base
  kcidb_tree_name: upstream-xfs

xfsprogs:
  description: Run tests on xfsprogs tree.
  .extends: .xfs-base
  kcidb_tree_name: upstream-xfs-xfsprogs
  watch_url: https://git.kernel.org/pub/scm/fs/xfs/xfsprogs-dev.git
  watch_branch: for-next
